﻿using UnityEngine;
using System;
using System.Collections;

public class GameController : MonoBehaviour {
  public static GameController S;
  public AudioClip successSound;
  public AudioClip failureSound;
  public AudioClip flipSound;

  public GameObject[] cards;
  public AudioClip successClip;
  public AudioClip failureClip;

  private static int NROWS = 4;
  private static int NCOLUMNS = 6;
  private AudioSource audioSource;

  void Awake() {
    S = this;
    audioSource = GetComponent<AudioSource>();
  }

	void Start() {
    // Shuffle.
    for (int i = 0; i < cards.Length; ++i) {
      GameObject tmp = cards[i];
      int ri = UnityEngine.Random.Range(i, cards.Length);
      cards[i] = cards[ri];
      cards[ri] = tmp;
    }

    // Layout.
    for (int i = 0; i < cards.Length; ++i) {
      GameObject card = Instantiate(cards[i]) as GameObject;
      card.transform.position = new Vector3(i % NCOLUMNS * 1.05f, i / NCOLUMNS * 1.3594f * 1.05f, 0);
    }  
	}
	
  private CardController cardA;
  private CardController cardB;
  private CardController cardC;

  public IEnumerator OnFlip(CardController card) {
    if (cardA == null) {
      cardA = card;
      yield return StartCoroutine(card.SetVisible(true));
      audioSource.PlayOneShot(flipSound, 1.0f);
    } else if (cardB == null) {
      cardB = card;
      yield return StartCoroutine(card.SetVisible(true));
      audioSource.PlayOneShot(flipSound, 1.0f);
    } else if (cardC == null) {
      cardC = card;
      yield return StartCoroutine(card.SetVisible(true));

      int[] worths = {cardA.GetWorth(), cardB.GetWorth(), cardC.GetWorth()};
      Array.Sort(worths);
    
      // We've got a Mathch!
      if (worths[0] + worths[1] == worths[2]) {
        audioSource.PlayOneShot(successClip, 1.0f);

        yield return new WaitForSeconds(2);

        Destroy(cardA.gameObject);
        Destroy(cardB.gameObject);
        Destroy(cardC.gameObject);
      } else {
        audioSource.PlayOneShot(failureClip, 1.0f);

        yield return new WaitForSeconds(2);
        yield return StartCoroutine(cardA.SetVisible(false));
        yield return StartCoroutine(cardB.SetVisible(false));
        yield return StartCoroutine(cardC.SetVisible(false));
      }

      cardA = cardB = cardC = null;
    }
  }
}
